#include"p18f4553.h"
#include "KeyPad.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
void operaciones(char*, float*,int);
void Delay(int ms)
{
	while(ms--)
	{
		int x=1500;
		while(x){
			x--;
		}
	}	
}

void inicializar(void)
{
	TRISB = 0x00;
	TRISD = 0x0f;
	PORTB = 0;
	PORTD= 0;
}

void operaciones(char * operacion, float* arreglo,int numeros){
    //(read == "*" || read == "A" || read == "B" || read = "C"
    float op1,op2;
    op1 = arreglo[numeros-1];
    op2 = arreglo[numeros];
    arreglo[numeros] = 0;
    switch ((int)operacion)
    {
        case '*':
            arreglo[numeros-1] = op1 * op2;
            break;
        case 'A':
            arreglo[numeros - 1] = op1 / op2;
            break;
        case 'B':
            arreglo[numeros - 1] = op1 + op2;
            break;
        case 'C':
            arreglo[numeros - 1] = op1 - op2;
            break;
        default:
            break;
    }
}

void run(void)
{
	unsigned char keyRead=0;
	unsigned char keyPress=0;
	unsigned char keyGet=0;
	float numero[8] = {0,0,0,0,0,0,0,0};
    int numeros = 0;
    char * num="";
	while(1)
	{   
        char* read;
        Delay(15);
		if(KB_Hit()){
            Delay(15);
            while(KB_Hit());
            read = KB_Get();
            if( (read <= "9" && read >= "0")){
                char* aux = read;
                aux = (char *)malloc(strlen(num) + 1);
                strcpy(aux, num);
                strcat(aux, read);
                num = (char *)malloc(strlen(aux) + 1);
                strcpy(num, aux);
            }else if(read == "#" && strlen(num) > 0 && numeros < 8){
                numero[numeros] = strtod(num, NULL);
                numeros++;
                strcpy(num, "");
            }else if( (read == "*" || read == "A" || read == "B" || read == "C") && numeros > 1){
                operaciones(read,numero,numeros);
                numeros --;
            }
        }
        printf("Resultado actual: %f\n",numero[numeros]);
	}
}

void main(void)
{
	inicializar();
	run();	
}