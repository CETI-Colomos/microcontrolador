#include <stdio.h>
#include <p18f4553.h>

unsigned int q(unsigned int,unsigned int);
unsigned int qJKnuev = 0, qJKant = 0;

void InitDevices(void)
{
    TRISB = 0x00;
    TRISD = 0xFF;
}
void InitApplication(void)
{
}
void Delay(int ms)
{
    while (ms--)
    {
        int x = 1500;
        while (x)
        {
            x--;
        }
    }
}
void Initialize(void)
{
    InitDevices();
    InitApplication();
}

unsigned int q(unsigned int j, unsigned int k){
    return (j & k) ? !qJKnuev : qJKand;
}

void Run(void)
{
    int flagJK=1,flagD=1;
    while(1){
        qJKant = qJKnuev;
        qJKnuev = PORTDbits.RD0;
        if(flagD & PORTDbits.RD4){
            flagD = 0;
        }else if(!flagD & !PORTDbits.RD4){
            LATBbits.RB1 = PORTDbits.RD3;
            flagD = 1;
        }
        if(flagJK & PORTDbits.RD1){
            flagJK = 0;
        }else if (!flagJK & !PORTDbits.RD1){
            LATBbits.RB0 = ((PORTDbits.RD0 & !PORTDbits.RD2) || (!PORTDbits.RD0 & PORTDbits.RD2)) ? PORTDbits.RD0 : q(PORTDbits.RD0,PORTDbits.RD2);
            flagJK = 1;
        }
    }
}

void Uninitialize(void)
{
}
void main(void)
{
    Initialize();
    Run();
    Uninitialize();
}
