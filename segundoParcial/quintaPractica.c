#include <stdio.h>
#include <p18f4553.h>

unsigned int bcd(unsigned int);

void InitDevices(void)
{
    TRISB = 0x00;
    TRISD = 0xFF;
}
void InitApplication(void)
{
}
void Delay(int ms)
{
    while (ms--)
    {
        int x = 1500;
        while (x)
        {
            x--;
        }
    }
}
void Initialize(void)
{
    InitDevices();
    InitApplication();
}
unsigned int bcd(unsigned int baseBinario){
    unsigned int bcd = 0, cambio = 0;
    while(baseBinario > 0){
        bcd |= (baseBinario %10) << (cambio++ << 2);
        baseBinario /= 10;
    }
    return bcd;
}


void Run(void)
{
    while(1){
        LATB = (unsigned char) bcd(PORTD);
    }
}

void Uninitialize(void)
{
}
void main(void)
{
    Initialize();
    Run();
    Uninitialize();
}
