#include <stdio.h>
#include <p18f4553.h>

unsigned char and(unsigned char , unsigned char);
unsigned char or(unsigned char , unsigned char);
unsigned char xor(unsigned char , unsigned char);
unsigned char add(unsigned char , unsigned char);
unsigned char sub(unsigned char , unsigned char);
unsigned char mul(unsigned char , unsigned char);
unsigned char shl(unsigned char , unsigned char);
unsigned char shr(unsigned char , unsigned char);

void InitDevices(void)
{
    TRISB = 0b01110000;
    TRISD = 0xFF;
}
void InitApplication(void)
{

}
void Delay(int ms)
{
	while(ms--)
	{
		int x=1500;
		while(x){
			x--;
		}
	}	
}
void Initialize(void)
{
    InitDevices();
    InitApplication();
}
unsigned char and(unsigned char port1, unsigned char port2){
    return (unsigned char) (port1 & port2);
}
unsigned char or(unsigned char port1, unsigned char port2){
    return (unsigned char) (port1 | port2);
}
unsigned char xor(unsigned char port1, unsigned char port2){
    return (unsigned char) (port1 ^ port2);
}
unsigned char  add(unsigned char port1, unsigned char port2){
    return (unsigned char) (port1 + port2);
}
unsigned char sub(unsigned char port1, unsigned char port2){
    return (unsigned char) (port1 - port2);
}
unsigned char mul (unsigned char port1, unsigned char port2){
    return (unsigned char) (port1 * port2);
}
unsigned char shl(unsigned char port1, unsigned char port2){
    return (unsigned char) (port1 << port2);
}
unsigned char shr(unsigned char port1, unsigned char port2){
    return (unsigned char) (port1 >> port2);
}

void Run(void)
{
    unsigned char(*opcion[8])(unsigned char, unsigned char) = {and, or, xor, add, sub, mul, shl, shr};
    while(1){
        LATB = (*opcion[(PORTB & 0b01110000)>>4])((PORTD & 0b11110000)>>4,(PORTD & 0b00001111));
    }
}

void Uninitialize(void){

}
void main(void){
    Initialize();
    Run();
    Uninitialize();
}
