#include<stdio.h>
#include<p18f4553.h>

void InitDevices(void)
{
    TRISB = 0x00;
    TRISD = 0x0F;
}
void InitApplication(void)
{

}
void Delay(int ms)
{
    while(ms--)
    {
        int x=1500;
        while(x){
            x--;
        }
    }   
}
void Initialize(void)
{
    InitDevices();
    InitApplication();
}


void Run(void)
{
    unsigned char patrones [4][8] = {
        {1,2,4,8,16,32,64,128},
        {128,64,32,16,8,4,2,1},
        {32,16,64,8,4,128,2,1},
        {48,72,132,3,3,132,72,48}
    };
  
    int velocidades [4] = {
        125,
        250,
        500,
        1000
    };
    
    while(1){
        int i;
        for(i = 0; i < 8; i++){
            LATB = patrones[(PORTD & 0b00000011)][i];
            Delay(velocidades[(PORTD & 0b00001100)>>2]);
        }
    }
}

void Uninitialize(void){

}
void main(void){
    Initialize();
    Run();
    Uninitialize();
}
