#include<stdio.h>
#include<p18f4553.h>


void InitDevices(void)
{
	TRISB = 0x00;
	TRISD = 0x03;
}
void InitApplication(void)
{

}
void Delay(int ms)
{
	while(ms--)
	{
		int x=1500;
		while(x){
			x--;
		}
	}	
}
void Initialize(void)
{
	InitDevices();
	InitApplication();
}
void Run(void)
{
	while(1){
		LATB  = (!PORTDbits.RD0) ? 255 : 0;
		do{
			Delay(500);
			LATB += (!PORTDbits.RD0) ? -1 : 1;
		}while(!PORTDbits.RD1);
	}
}
void Uninitialize(void){

}
void main(void){
	Initialize();
	Run();
	Uninitialize();
}