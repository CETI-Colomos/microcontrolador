#include <stdio.h>
#include <p18f4553.h>

double raiz_cuadrada(double, double, int);

void InitDevices(void)
{
    TRISB = 0x00;
    TRISD = 0xFF;
}
void InitApplication(void)
{
}
void Delay(int ms)
{
    while (ms--)
    {
        int x = 1500;
        while (x)
        {
            x--;
        }
    }
}
void Initialize(void)
{
    InitDevices();
    InitApplication();
}
double raiz_cuadrada(double numero, double aproximacion, int n)
{
    double aproximacionantes = aproximacion;

    aproximacion = aproximacion - (((aproximacion * aproximacion) - numero) / (2 * aproximacion));

    if (aproximacion == aproximacionantes || n > 50)
        return aproximacion; 

    return raiz_cuadrada(numero, aproximacion, ++n);
}
void Run(void)
{
    
    while(1){
        LATB = (unsigned int)raiz_cuadrada((double)PORTD, 1, 0);
    }
}

void Uninitialize(void)
{
}
void main(void)
{
    Initialize();
    Run();
    Uninitialize();
}
