#include <stdio.h>
#include <p18f4553.h>

unsigned int bcd(unsigned int);

void InitDevices(void)
{
    TRISB = 0b00001111;
    TRISD = 0b00000011;
}
void InitApplication(void)
{
}
void Delay(int ms)
{
    while (ms--)
    {
        int x = 1500;
        while (x)
        {
            x--;
        }
    }
}
void Initialize(void)
{
    InitDevices();
    InitApplication();
}
unsigned int bcd(unsigned int baseBinario){
    unsigned int bcd = 0, cambio = 0;
    while(baseBinario > 0){
        bcd |= (baseBinario %10) << (cambio++ << 2);
        baseBinario /= 10;
    }
    return bcd;
}

void Run(void)
{
    //unsigned int bin [8] = {0b00000001,0b0000101,0b00000011,0b00000001,0b00000000,0b00000000,0b00001000,0b00000110};
    unsigned char bin [8] = {1,5,3,1,0,0,4,6};
    int flag = 1,i = 0;
    while(1){
        i = (PORTDbits.RD1) ? 0 : i;
        if(PORTDbits.RD0 && flag && i < 8){
            i ++;
            flag = 0;
        }else if(!PORTDbits.RD0 && !flag){
            flag = 1;
        }
        LATB = (unsigned char)bcd((unsigned int)bin[i]);
    }
}

void Uninitialize(void)
{
}
void main(void)
{
    Initialize();
    Run();
    Uninitialize();
}
